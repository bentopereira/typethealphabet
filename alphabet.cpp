//#include <stdio.h>
//#include <stdlib.h>
//#include <sys/time.h>
#include <bits/stdc++.h>

using namespace std;

int main () {

	printf("Digite o alfeto (em minusculas) o mais rápido que puder! Valendo!\n");
	
	string alfabeto = "abcdefghijklmnopqrstuvwxyz";
	string input;

	struct timespec start, end;

	clock_gettime (CLOCK_MONOTONIC_RAW, &start);
	
	getline (cin, input);

	clock_gettime (CLOCK_MONOTONIC_RAW, &end);

	unsigned long long delta = (end.tv_sec - start.tv_sec) *1e6 + 
(end.tv_nsec - start.tv_nsec) / 1000;


	if (alfabeto == input){
		printf("Ganhou!\nSeu tempo foi %lf segundos\n", double(delta)/1e6);
	}
	else{

		printf("Perdeu!\nVoce demorou %lf segundos mas ainda nao deu!\n", double(delta)/1e6);

		if(input.size() < alfabeto.size()){
			printf("Parece que faltaram algumas letras!\n");
		}
		else if(input.size() > alfabeto.size()){
			printf("Ops! O alfabeto nao tem tanta letras!\n");
		}

		printf("Aqui estao seus erros\n");

		int i;

		for(i = 0; alfabeto[i] != '\0' && input[i] != '\0'; i++){
			if(alfabeto[i] != input[i]){

				printf("%c", toupper(input[i]));

			}
			else{

				printf("%c", input[i]);

			}
		}

		while(input[i] != '\0'){
			printf("%c", toupper(input[i++])); //imprime letras adicionais que o usuario colocou como erradas
		}

		printf("\n");
	}

	return 0;
}
